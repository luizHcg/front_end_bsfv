# bsecomfv

## Project setup
```
npm install
Depois subistitua a na pasta "node_modules/bootstrap/scss/_variables.scss" com arquivo "_variables" 
dentro de "src\app\views\style\bootstrap-variables"
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
