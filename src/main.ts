import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "@/app/views/style/bs-style/BSMain.scss";

import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
Vue.use(BootstrapVue, BootstrapVueIcons);

// Componentes personalizados
import BSNavBar from "@/app/views/components/header/bs-nav-bar.vue";
Vue.component("bs-nav-bar",BSNavBar);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
