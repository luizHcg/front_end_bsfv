import ProdutosClass from "@/app/model/Produtos.class";
import FreteClass from "@/app/model/Frete.class";

class ForcaVendaController {

    public SumListProdutos(list: Array<ProdutosClass>): number {

        let value = 0;

        list.forEach(element => {
            if (element.preco && element.quantidade)
                value += (element.quantidade * element.preco)
        });

        return value;
    }

}

export default new ForcaVendaController()
