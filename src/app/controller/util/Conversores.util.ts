class ConversoresUtil {

    public ConverterEmReal(valor: number) {
        return valor.toLocaleString('pt-br', {
            style: 'currency',
            currency: 'BRL'
        });
    }

}

export default new ConversoresUtil()
