import Vue from "vue";

class ConfirmationModal extends Vue {
    public async confirmModal(msg: string) {
        let result: boolean | string = false;

        await this.$bvModal
            .msgBoxConfirm(msg, {
                headerBgVariant: "dark",
                headerTextVariant: "light",
                title: "Deseja proceguir ?",
                buttonSize: "sm",
                okVariant: "outline-dark",
                okTitle: "Confirmar",
                cancelVariant: "outline-danger",
                cancelTitle: "Cancelar",
                footerClass: "p-2",
                hideHeaderClose: false,
                centered: true
            })
            .then((value: boolean) => {
                result = value;
            })
            .catch(({message}) => {
                result = message;
            });
        return result !== null ? result : false;
    }

    public WarningModal() {
        this.$bvToast.toast("Toast body content", {
            title: "Alerta!!",
            toaster: "b-toaster-top-full",
            variant: "warning",
            solid: true
        });
    }

    public ErrorModal(message: string) {
        this.$bvToast.toast(message, {
            title: "Error!!",
            variant: "danger",
            solid: true,
            noAutoHide: true
        });
    }
}

export default ConfirmationModal;
