import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Autenticacao from "@/app/views/components/usuario/autenticacao/Autenticacao.vue";
import ForcaVenda from "@/app/views/components/forca-venda/ForcaVenda.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: process.env.VUE_APP_ROTA_USUARIO_AUTENTICACAO,
    name: "Autenticacao",
    component: Autenticacao
  },
  {
    path: process.env.VUE_APP_ROTA_FORCA_VENDA,
    name: "Força de vendas",
    component: ForcaVenda
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
